<?php

namespace App\Http\Controllers;

use App\Models\Chirp;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class ChirpController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Response 
    {
        return Inertia::render('Chirps/Index', [
            //
        ]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // No need to validate here, as validation should be done in the store method.
        return Inertia::render('Chirps/Create', [
            //
        ]);
    }
    
    public function store(Request $request): RedirectResponse
    {
        // Validate the incoming request data
        $validated = $request->validate([
            'message' => 'required|string|max:255',
        ]);
    
        // Create a new Chirp using the authenticated user's relationship
        $request->user()->chirps()->create($validated);
    
        // Redirect to the index route for chirps
        return redirect(route('chirps.index'));
    }
    
    /**
     * Display the specified resource.
     */
    public function show(Chirp $chirp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Chirp $chirp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Chirp $chirp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Chirp $chirp)
    {
        //
    }
}
